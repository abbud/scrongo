import mongoengine as me

class Card(me.Document):
        scryfall_id = me.fields.StringField(primary=True)
        oracle_id = me.fields.StringField()
        name = me.StringField()
        scryfall_uri = me.StringField()
        layout = me.StringField()
        image_uris = me.DictField()
        mana_cost = me.StringField()
        cmc = me.FloatField()
        oracle_text = me.StringField()
        colors = me.ListField()
        color_identity = me.ListField()
        legalities = me.DictField()
        set = me.StringField()
        set_name = me.StringField()
        scryfall_set_uri = me.StringField()
        rarity = me.StringField()
        related_uris = me.DictField()
        artist = me.StringField()
        meta = {'strict': False}



