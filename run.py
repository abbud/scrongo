"""
Scrongo, from ScryfallAPI to Mongo.

Usage:
        run.py <database_name>
"""

import logging
import requests
import json
import mongoengine as me
from docopt import docopt
from progress.bar import Bar

from models import Card

def scryfall_json_to_dict(scryfall_json_card):
        return {
                "scryfall_id":scryfall_json_card.get("id"),
                "oracle_id":scryfall_json_card.get("oracle_id"),
                "name":scryfall_json_card.get("name"),
                "scryfall_uri":scryfall_json_card.get("scryfall_uri"),
                "layout":scryfall_json_card.get("layout"),
                "image_uris":scryfall_json_card.get("image_uris"),
                "mana_cost":scryfall_json_card.get("mana_cost"),
                "cmc":scryfall_json_card.get("cmc"),
                "oracle_text":scryfall_json_card.get("oracle_text"),
                "colors":scryfall_json_card.get("colors"),
                "color_identity":scryfall_json_card.get("color_identity"),
                "legalities":scryfall_json_card.get("legalities"),
                "set":scryfall_json_card.get("set"),
                "set_name":scryfall_json_card.get("set_name"),
                "scryfall_set_uri":scryfall_json_card.get("scryfall_set_uri"),
                "rarity":scryfall_json_card.get("rarity"),
                "artist":scryfall_json_card.get("artist"),
                "related_uris":scryfall_json_card.get("related_uris"),
        }

def fetch_default_cards_uri(requests):
        BULK_DATA_INFO = 'https://api.scryfall.com/bulk-data'
        r = requests.get(BULK_DATA_INFO).json()
        data = r['data']
        return  next(
                filter(lambda x: x['type'] == 'default_cards', data)
        )['permalink_uri']

def download_from_scryfall(default_cards_uri, requests, chunk_size=128):
        default_cards = requests.get(default_cards_uri, stream=True)
        chunk_length = int(default_cards.headers['Content-Length'])/chunk_size
        bar = Bar('Downloading data from ScryfallAPI',
                  max=chunk_length,
                  suffix='%(percent)d%%')
        with open('scryfall.temp', 'wb') as fd:
                for chunk in default_cards.iter_content(chunk_size=chunk_size):
                        fd.write(chunk)
                        bar.next()
        bar.finish()
        return json.load(open('scryfall.temp'))


def dump_to_mongo(cards, me):
        bar = Bar('Saving cards info in MongoDB', max=len(cards))
        for card in cards:
                Card(**scryfall_json_to_dict(card)).save()
                bar.next()
        bar.finish()

def main():
        database_name = docopt(__doc__)['<database_name>']
        me.connect(database_name)
        uri = fetch_default_cards_uri(requests)
        cards = download_from_scryfall(uri, requests)
        dump_to_mongo(cards, me)

main()
